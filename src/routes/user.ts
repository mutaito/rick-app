import { Router } from 'express';
import { userController } from '../controllers/UserController';

class UserRoutes {

    public router: Router = Router();

    constructor() {
        this.configRoutes();
    }

    configRoutes(): void {
        this.router.get('/', userController.getUsers);
        this.router.post('/add', userController.saveUser);
        this.router.delete('/delete', userController.deleteUser);
        this.router.patch('/update', userController.updateUser);
    }

}

const userRoutes: Router = new UserRoutes().router;
export default userRoutes;