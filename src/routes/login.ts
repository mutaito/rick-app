import { Router } from 'express';
import { loginController } from '../controllers/LoginController';

class LoginRoutes {

    public router: Router = Router();

    constructor() {
        this.configRoutes();
    }

    configRoutes(): void {
        //this.router.get('/', userController.getUsers);
        this.router.post('/', loginController.login);
        //this.router.delete('/delete', userController.deleteUser);
        //this.router.patch('/update', userController.updateUser);
    }

}

const loginRoutes: Router = new LoginRoutes().router;
export default loginRoutes;