import { Router } from 'express';
import { rickAndMortyController } from '../controllers/RickAndMortyController';

class RickAndMortyRoutes {

    public router: Router = Router();

    constructor() {
        this.configRoutes();
    }

    configRoutes(): void {
        this.router.get('/rickandmorty', rickAndMortyController.getCharacters);
    }

}

const rickAndMortyRoutes: Router = new RickAndMortyRoutes().router;
export default rickAndMortyRoutes;