import { RedisConfig } from './redisConfig';


class Redis {
    private db: any;
    constructor() {

        this.initDb();
    }
    private initDb() {
        try {
            this.db = RedisConfig.getInstance().getConnect();
        } catch (ee) {
            console.log(ee);
        }
    }
    pushList(item: any, key: any) {
        return new Promise((resolve, reject) => {
            this.db.RPUSH(key, item, (err: any, val: any) => {
                if (err)
                    reject(err)
                else
                    resolve(val)
            })
        })
    }
    async findUser(key: any, username: any) {
        const users: any = await this.getList(key);
        const exists = users.find((user: any) => { JSON.parse(user).username === username })
        return exists
    }

    getList(key: any) {
        /*const instancia1 = RedisConfig.getInstance().getConnect();
        const instancia2 = RedisConfig.getInstance().getConnect();
        console.log(instancia1 === instancia2);
        console.log(instancia1);
        */
        return new Promise((resolve, reject) => {
            this.db.lrange(key, 0, -1, (err: any, val: any) => {
                if (err)
                    reject(err)
                else
                    resolve(val)
            })
        });
    }

    setItem(key: any, item: any, tExpSec: any) {
        return new Promise((resolve, reject) => {
            this.db.SETEX(key, tExpSec, item, (err: any, val: any) => {
                if (err)
                    reject(err)
                else
                    resolve(val)
            })
        })
    }

    find(criterie: any) {
        return new Promise((resolve, reject) => {
            this.db.KEYS(criterie, (err: any, items: any) => {
                if (err)
                    reject(err)
                else
                    resolve(items)
            })
        })
    }
}
export const { find, findUser, getList, pushList, setItem } = new Redis(); 