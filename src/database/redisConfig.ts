import redis from 'redis';
//const redis = require('redis');
const URL: any = process.env.REDIS_URL || 6379;

export class RedisConfig {
    private client: any;
    private static instance: RedisConfig;
    constructor() {
        console.log("RedisConfig Constructor....")
        console.log(this)
        this.startCon();
    }
    private startCon(){
        try{
            this.client = redis.createClient(URL);
            if(!this.client.connected){
               console.log("Error")
               throw new Error('Error, Redis') 
            }
        }catch(err){
            console.log(err)
        }
    }
    static getInstance(): RedisConfig {
        if (!RedisConfig.instance) {
            //console.log("getInstance()")
            RedisConfig.instance = new RedisConfig();
        }
        //console.log("getInstance()::")
        return RedisConfig.instance
    }
    public getConnect():RedisConfig{
        return this.client
    }
}