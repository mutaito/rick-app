import express from 'express';
import methodOverride from 'method-override';
import {cartoon, user,login} from './routes/index';
var jwt = require('express-jwt');

const app = express();
// SETTINGS
app.set('port', process.env.PORT || 3002);

// MIDDLEWARES
//app.use(jwt({ secret: 'secreto'}).unless({path: ['/login']}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride());



// ROUTES
app.use('/users', user);
app.use('/cartoon', cartoon);
app.use('/login', login);

//SERVER
app.listen(app.get('port'), () => {
    console.log(`Server running on port ${app.get('port')}`);
})