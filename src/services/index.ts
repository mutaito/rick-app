import { Request } from "express";
import fetch from "node-fetch";
const { rickAndMortyAPI } = require('../keys');

export interface IInfo {
    count: number;
    pages: number;
    next: string;
    prev: string | null;
}

export interface ICharactes {
    id: number,
    name: string,
    status: string,
    species: string,
    gender: string,
    image: string;
}


export interface IRickAMResponse {
    info: IInfo;
    results: ICharactes[];
}
/*
export interface ICharactesResponse {
    results: ICharactes[];
}*/
class Services {
    public async getRickAndMortyCharacters(req?: Request): Promise<IRickAMResponse> {
        let res = await fetch(rickAndMortyAPI.URI);
        if (!res) {
            throw new Error('Cant get getRickAndMortyCharacters')
        }
        return res.json() as Promise<IRickAMResponse>
    }
}
export const services = new Services();