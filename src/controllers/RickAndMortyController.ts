import { Response, Request } from "express";
import { services, ICharactes } from "../services";

const reduceResponse = (array: ICharactes[]) => {

    return array.map((item: ICharactes) => {
        return {
            id: item.id,
            name: item.name,
            status: item.status,
            species: item.species,
            gender: item.gender,
            image: item.image
        };
    });
}

class RickAndMortyController {
    public async getCharacters(req: Request, res: Response): Promise<void> {
        const serviceResponse = await services.getRickAndMortyCharacters(req);
        const response = reduceResponse(serviceResponse.results);
        res.json(response);
    }
}
export const rickAndMortyController = new RickAndMortyController();