import { Response, Request } from "express";
import { find, findUser, getList, pushList, setItem } from "../database/redis";
const jwt = require('jsonwebtoken')

class LoginController {

    public async login(req: Request, res: Response) {
        const { username, password } = req.body
        const users: any = await getList('lusers');
        console.log(users)
        const user = users.find((user: any) => {
            const u = JSON.parse(user)
            return u.username === username && u.password === password
        })

        if (user) {
            const exp = 120;
            const token = jwt.sign({ sub: username }, process.env.secret, {
                algorithm: 'HS256',
                expiresIn: exp
            })

            await setItem('authenticated:' + username, user, exp)
            res.send({ message: 'Usuario autenticado exitosamente', token })
            return res.send({ test: "sdgsdgs" })
        }
    }
    public async logout(req: Request, res: Response) {
        
    }

    public async userAuth(req: Request, res: Response) {
        return res.json({})
    }
}
export const loginController = new LoginController();      