import { Response, Request } from "express";
import { find, findUser, getList, pushList, setItem } from "../database/redis";

class UserController {
    //public userList:string;
    
    public async getUsers(req: Request, res: Response) {
        const users = await getList('lusuarios');
        return res.send(users)
    }
    public async saveUser(req: Request, res: Response) {
        try {
            const stUser = JSON.stringify(req.body)
            if (stUser) {
                const username = req.body.username
                const registeredUser = await findUser('lusuarios', username)
                if (registeredUser) {
                    res.statusCode = 409
                    res.send('Usuario ya existe.')
                } else {
                    await pushList(stUser, 'lusuarios')
                    res.statusCode = 201
                    res.send('Usuario registrado exitosamente')
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    public async deleteUser(req: Request, res: Response) {
        return res.json({})
    }

    public async updateUser(req: Request, res: Response) {
        return res.json({})
    }
}
export const userController = new UserController();      